package mgrhttp

import (
	"encoding/json"
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"strconv"

	config "csudata.com/zqpool/src/config"
	poolserver "csudata.com/zqpool/src/poolserver"
	"go.uber.org/zap"
)

const ApiSuccess = 0
const ApiBadRequest = 1
const ApiSessionTimeOut = 2
const ApiServerError = -1

func httpHandlePoolAddBeDb(w http.ResponseWriter, r *http.Request) {
	var err error

	if err = r.ParseForm(); err != nil {
		w.Write([]byte(fmt.Sprintf(`{"code":%d, "message": %s}`, ApiBadRequest, strconv.Quote(err.Error()))))
		return
	}

	// ck, err := r.Cookie("sessionid")
	// if err != nil {
	// 	w.Write([]byte(fmt.Sprintf(`{"code":%d, "message": %s}`, ApiSessionTimeOut, "session timeout ")))
	// 	return
	// }
	// fmt.Printf("ck=%s\n", ck.Value)
	poolName := r.PostForm.Get("pool_name")
	dbPortal := r.PostForm.Get("db_portal")

	err = config.PoolAddBeDb(poolName, dbPortal)
	if err != nil {
		w.Write([]byte(fmt.Sprintf("{\"code\":%d, \"err_msg\": %s}", -1, err.Error())))
	} else {
		w.Write([]byte(fmt.Sprintf("{\"code\":%d}", 0)))
	}
}

func httpHandlePoolRemoveBeDb(w http.ResponseWriter, r *http.Request) {
	var err error

	if err = r.ParseForm(); err != nil {
		w.Write([]byte(fmt.Sprintf(`{"code":%d, "message": %s}`, ApiBadRequest, strconv.Quote(err.Error()))))
		return
	}

	// ck, err := r.Cookie("sessionid")
	// if err != nil {
	// 	w.Write([]byte(fmt.Sprintf(`{"code":%d, "message": %s}`, ApiSessionTimeOut, "session timeout ")))
	// 	return
	// }
	// fmt.Printf("ck=%s\n", ck.Value)
	poolName := r.PostForm.Get("pool_name")
	dbPortal := r.PostForm.Get("db_portal")

	err = config.PoolRemoveBeDb(poolName, dbPortal)
	if err != nil {
		w.Write([]byte(fmt.Sprintf("{\"code\":%d, \"err_msg\": %s}", -1, err.Error())))
		return
	}

	err = poolserver.PoolReleaseBeDb(poolName, dbPortal)
	if err != nil {
		w.Write([]byte(fmt.Sprintf("{\"code\":%d, \"err_msg\": %s}", -1, err.Error())))
	} else {
		w.Write([]byte(fmt.Sprintf("{\"code\":%d}", 0)))
	}
}

func httpHandlePoolListBeDb(w http.ResponseWriter, r *http.Request) {
	var err error

	if err = r.ParseForm(); err != nil {
		w.Write([]byte(fmt.Sprintf(`{"code":%d, "message": %s}`, ApiBadRequest, strconv.Quote(err.Error()))))
		return
	}

	// ck, err := r.Cookie("sessionid")
	// if err != nil {
	// 	w.Write([]byte(fmt.Sprintf(`{"code":%d, "message": %s}`, ApiSessionTimeOut, "session timeout ")))
	// 	return
	// }
	// fmt.Printf("ck=%s\n", ck.Value)
	poolName := r.PostForm.Get("pool_name")

	portalList, err := config.PoolGetBeDbList(poolName)
	if err != nil {
		w.Write([]byte(fmt.Sprintf("{\"code\":%d, \"err_msg\": %s}", -1, err.Error())))
	} else {
		strList, _ := json.Marshal(portalList)
		w.Write([]byte(fmt.Sprintf("{\"code\":%d, \"be_db_list\": %s}", 0, strList)))
	}
}

func StartHttpServer() {

	var err error

	webPort := config.GetInt("mgr_port")
	var httpListenAddr = fmt.Sprintf(":%d", webPort)
	http.HandleFunc("/api/v1/pool_add_be_db", httpHandlePoolAddBeDb)
	http.HandleFunc("/api/v1/pool_remove_be_db", httpHandlePoolRemoveBeDb)
	http.HandleFunc("/api/v1/pool_list_be_db", httpHandlePoolListBeDb)

	zap.S().Infof("Manager http Listen %s", httpListenAddr)
	err = http.ListenAndServe(httpListenAddr, nil)
	if err != nil {
		zap.S().Error("Manager http listen error : ", err)
	}
}
